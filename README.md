# Rude Click

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

I made this as a way to learn animation and pixel art in Krita and people seemed to like it so I made this repo to distribute it.

Everything here is CC-BY.

### Fast Version

<img src="Bad%20Click%20Fast.gif" alt="Bad Click Fast" style="height: 200px;"/>

### Slow Version

<img src="Bad%20Clickv2.gif" alt="Bad Click" style="height: 200px;"/>
